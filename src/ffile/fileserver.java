package ffile;

import java.io.*;
import java.net.*;


public class fileserver {
    public static void main(String[] args) throws Exception{

        ServerSocket s = new ServerSocket(4333);
        Socket sr = s.accept();

        File myFile = new File ("/Users/pro/Downloads/benny.jpg");

        // je lis un fichier depuis un dossier dont la localisation est précisée.
        FileInputStream fr=new FileInputStream(myFile);


        byte [] mybytearray =new byte [(int)myFile.length()];
        fr.read(mybytearray, 0, mybytearray.length);
        OutputStream os =sr.getOutputStream(); //outputstream utilisé pour convertir
                                                // le fichier vers un format stream

        os.write(mybytearray,0,mybytearray.length);
        os.flush(); // dire à l'OS d'envoyer dans le câble tout ce qu'il y a à envoyer
        sr.close(); // fermeture de la socket

    }
}
